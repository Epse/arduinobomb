# Arduinobomb
**! For English, see README_EN.md !**

De Arduinobom is een simpele bom waarbij je om ze te ontmantelen (in bv. een spel of escape room) je twee sleutels (of andere schakelaars) en 10 codes nodig hebt.
We gaan er vanuit dat de spelers de manier waarop codes gecheckt worden niet uitvogelen.

## Opstarten
Wanneer de bom stroom krijgt (in mijn model door ze voorzichtig open te draaien en de 9V batterij aan te sluiten) verschijnt er op de LCD een prompt.
Hier kan je kiezen hoeveel tijd je krijgt voor de bom ontploft.

Je kan de tijd (in minuten) intypen via het toetsenbord (numpad of de cijfers boven de letters, geen shift gebruiken) (je mag nul gebruiken).
Wanneer je alle drie de cijfers hebt ingetypt springt de cursor automatisch weer naar de honderdtallen. Typ je dan weer een cijfer, wordt het hele getal gewist.

Wanneer je op TAB drukt, start de bom en begint de timer te lopen.

## Tijdens het spel
Op de bovenste regel van de LCD staat een timer die de minuten en seconden aftelt. (Geen uren, twee uur toont als 120 minuten)
Op de tweede regels staad een sterretje per code die nog moet ingetoetst worden (begint bij 10) en een icoontje van een slot per slot dat nog niet geactiveerd is.

Om een code in te voeren druk je op ESC en typ je de code in, gevolgd door ENTER om te bevestigen.
BACKSPACE wist de volledige code. Nadat je bevestigd hebt gaat de bom terug naar het hoofdscherm voor een correcte code, 
of toont het eventjes "Foute Code" wanneer uw code foutief was. De bom onthoud de 20 laatst gebruikte codes en wanneer een van deze gebruikt wordt, toont de bom ook "Foute Code".

Wanneer een slot wordt geactiveerd verdwijnt automatisch het slot icoontje.

Is de tijd om en is de bom niet ontmijnd, dan verschijnt er een boze smiley en een tekst die je zegt dat je dood bent.
Slaag je er in de bom te ontmijnen, dan verschijnt bovenaan je uiteindelijke tijd en onderaan de tekst "VEILIG" gevolgd door enkele smileys.
Je kan na het ontmijnen nog steeds een slot terug deactiveren en de timer zal opnieuw beginnen lopen.

## Codes
De bom accepteert codes om sterretjes uit te schakelen en één code om sterretjes bij te plaatsen.

De code *1590* zet een sterretje bij.

Elke code die gevormd wordt volgens een bepaald patroon zet een sterretje uit.
Deze codes vorm je als volgt:

1. Kies 3 willekeurige cijfers. Bv.: 5, 8 en 2
2. Neem de som van deze cijfers. Bv.: 15
3. Plaats het laatste cijfer van de som (bv. 5) achter de drie willekeurige cijfers om de code te krijgen (bv. 5825)

**Let op!** de volgorde van de cijfers is dus van belang. (De eerste 3 cijfers mogen echter willekeurig gewisseld worden en geven telkens een geldige code bv. 5285)
