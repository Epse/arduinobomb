#include <PS2Keyboard.h>
#include <LiquidCrystal.h>

#define PS2_CLK_PIN 3
#define PS2_DATA_PIN 10
#define KEY_1_PIN 8
#define KEY_2_PIN 9
// Maybe an LED and an LSR that you slide a card in between?

LiquidCrystal lcd(12,11,5,4,6,2);
PS2Keyboard keyboard;
long long timeLeft = 60*60 ;
long lastSecond;
bool hasExploded = false;
byte challengesToGo = 10;
bool isInMenu = false;
byte currentCodeIndex = 0;
byte currentCode [4];
byte usedCodes [20][4];
byte usedCodesIndex = 0;
bool lock1 = true;
bool lock2 = true;
bool running = false;
byte minutes = 0;
bool tenMinutesSet = false;
bool hundredMinutesSet = false;

byte happy[8] = {
  B00000,
  B10001,
  B00000,
  B00000,
  B10001,
  B01110,
  B00000,
};

byte evil[8] = {
  B10001,
  B01010,
  B01010,
  B00000,
  B00000,
  B01110,
  B10001,
};

byte lock[8] = {
  B00000,
  B01110,
  B10001,
  B10001,
  B11111,
  B11111,
  B00000,
};

void clearCode() {
  for (byte i = 0; i < 4; i++) {
    currentCode[i] = 0;
  }
}

void recordUsedCode() {
  for (byte i = 0; i < 4; i++) {
    usedCodes[usedCodesIndex][i] = currentCode[i];
  }
  if (usedCodesIndex < 20) {
    usedCodesIndex++;
  } else {
    usedCodesIndex = 0;
  }
}

void setup() {
  keyboard.begin(PS2_DATA_PIN, PS2_CLK_PIN);

  pinMode(KEY_1_PIN, INPUT);
  pinMode(KEY_2_PIN, INPUT);

  lcd.begin(16,2);
  lcd.createChar(0, happy);
  lcd.createChar(1, evil);
  lcd.createChar(2, lock);
  lcd.clear();
  lcd.print("STANDBY ");
  lcd.write(byte(2));
  lcd.setCursor(0,1);
  lcd.print("___ min. TAB.");
}

void printCode() {
  if (!isInMenu)
    return;
  lcd.setCursor(0,1);
  lcd.print("____             "); // Clear this line
  lcd.setCursor(0,1);
  for (byte i = 0; i < currentCodeIndex; i++) {
    lcd.print(currentCode[i]);
  }
}

void redraw() {
  if (hasExploded)
    return;
  printTimeLeft();
  printChallenges();
  printCode();
}

void printTimeLeft() {
  int minutesLeft = timeLeft / 60;
  int secondsLeft = timeLeft % 60;
  lcd.setCursor(0,0);
  lcd.print(minutesLeft);
  lcd.print(":");
  lcd.print(secondsLeft);
  // Spaties zijn om achtergebleven karakters te wissen
  lcd.print(" te gaan.    ");
  if (timeLeft % 60 == 0) {
    lcd.setCursor(15,1);
    lcd.write(byte(1));
  }
  if (timeLeft % 60 == 59) {
    lcd.setCursor(15,1);
    lcd.write(byte(0));
  }
}

void printChallenges() {
  if (isInMenu)
    return;
  lcd.setCursor(0,1);
  for (byte i = 0; i < challengesToGo; i++) {
    lcd.print("*");
  }
  if (lock1) {
    lcd.write(byte(2));
  }
  if (lock2) {
    lcd.write(byte(2));
  }
  lcd.print("               ");

  if (challengesToGo < 1 && !lock1 && !lock2) {
    lcd.setCursor(0,1);
    lcd.print("VEILIG ");
    lcd.write(byte(0));
    lcd.write(byte(0));
  }
}

void explode() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("BOEM!          ");
  lcd.write(byte(1));
  lcd.setCursor(0,1);
  lcd.print("Je bent ontploft");
  hasExploded = true;
}

bool checkCodeUsed() {
  for (int i = 0; i < usedCodesIndex; i++) {
    if (currentCode[0] == usedCodes[i][0] && currentCode[1] == usedCodes[i][1] && currentCode[2] == usedCodes[i][2] && currentCode[3] == usedCodes[i][3]) {
      return true;
    }
  }
  return false;
}

void loop() {
  if (!running) {
    if (keyboard.available()) {
      byte dat = keyboard.read();
      byte val = dat - '0';
      if (dat == PS2_TAB) {
        running = true;
      } else if (val >= 0 && val <= 9) {
        // If the hundreds are not typed (probably nothing is)
        if (!hundredMinutesSet) {
          minutes = 100 * val;
          lcd.setCursor(0,1);
          lcd.print(val);
          lcd.print("__");
          hundredMinutesSet = true;
        } else if (!tenMinutesSet) {
          // We are typing the tens of minutes
          minutes += 10 * val;
          lcd.setCursor(1,1);
          lcd.print(val);
          lcd.print("_");
          tenMinutesSet = true;
        } else {
          // We are typing the minutes
          minutes += val;
          lcd.setCursor(2,1);
          lcd.print(val);
          timeLeft = minutes*60;
          hundredMinutesSet = false;
          tenMinutesSet = false;
        }
      }
    }
    return;
  }

  lock1 = digitalRead(KEY_1_PIN) == LOW;
  lock2 = digitalRead(KEY_2_PIN) == LOW;

  if (( challengesToGo > 0 || lock1 || lock2) && timeLeft > 0 && millis() - lastSecond >= 1000) {
    timeLeft--;
    lastSecond = millis();
  } else if (!hasExploded && timeLeft <= 0) {
    explode();
    return;
  }

  if(keyboard.available()) {
    byte dat = keyboard.read();
    byte val = dat - '0';

    if (isInMenu && dat == PS2_ESC) {
      isInMenu = false;
    }

    if (!isInMenu && dat == PS2_ESC) {
      isInMenu = true;
      currentCodeIndex = 0;
      clearCode();
    }

    if (isInMenu && val >= 0 && val <= 9) {
      currentCode[currentCodeIndex] = val;
      currentCodeIndex += 1;
    }

    if (isInMenu && dat == PS2_BACKSPACE) {
      clearCode();
      currentCodeIndex = 0;
    }

    if (isInMenu && dat == PS2_ENTER && currentCodeIndex > 3) {
      byte sum = 0;
      if (currentCode[0] == 1 && currentCode[1] == 5 && currentCode[2] == 9 && currentCode[3] == 0){
        challengesToGo += 1;
        isInMenu = false;
        redraw();
        return;
      }
      for (byte i = 0; i < 3; i++) {
        sum += currentCode[i];
      }
      if (sum % 10 == currentCode[3] && !checkCodeUsed()) {
        challengesToGo -= 1;
        isInMenu = false;
        recordUsedCode();
      } else {
        lcd.setCursor(0,1);
        lcd.print("Foute Code");
        delay(1000);
        isInMenu = false;
      }
    }
  }
  redraw();
}
